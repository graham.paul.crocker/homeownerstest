# Gpcrocker/Homeowner Test
 
## Description

Accepts a CSV and outputs an array of people, splitting the name into
the correct fields, and splitting multiple people from one string where appropriate.

 
## Installation
 
Run composer install
  
  `composer install`
  
## Executables
 
### Run PHPUnit Tests 
 
`chmod +x runTests && ./runTests`
  
### Run CLI

`php run.php`

### Unit test results

```Parser (Gpcrocker\Homeowners\Tests\Parser)
 ✔ Parser function with no·name·supplied
 ✔ Parser function with name·with·initial·and·surname
 ✔ Parser function with first·and·surname
 ✔ Parser function with first·and·surname·2
 ✔ Parser function with two·full·names
 ✔ Parser function with two·titles·and·one·surname
```
