<?php

use Gpcrocker\Homeowner\Parser;

require_once "bootstrap.php";

echo 'Homeowner Parser' . PHP_EOL;

if (PHP_SAPI !== 'cli') {
    echo <<<HTML
	<div style="font:12px/1.35em arial, helvetica, sans-serif;">
		<div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
			<h2>A Command Line App</h2>
			<h3>Should not be accessible to the public</h3>
		</div>
	</div>
HTML;
    exit(1);
}

const DATA_FILENAME = __DIR__ . '/data/examples.csv';
const VERBOSE = true;

$parser = new Parser();
$homeowners = $parser->parseCsvData(DATA_FILENAME);

if (VERBOSE) {
    var_dump($homeowners);
}