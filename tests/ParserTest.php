<?php

namespace Gpcrocker\Homeowners\Tests;

require_once __DIR__ . '/../bootstrap.php';

use Gpcrocker\Homeowner\Parser;
use Gpcrocker\Homeowner\Person;

use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    /**
     * @var Parser
     */
    private $parser;

    /**
     * @param string $name
     * @param array $expectedResult
     * @dataProvider nameProvider
     */
    public function testParserFunction(string $name, array $expectedResult)
    {
        $parser = $this->getParser();
        $computedResult = $parser->parsePerson($name);
        $this->assertSame($computedResult, $expectedResult);
    }

    /**
     * @return array[]
     */
    public function nameProvider()
    {
        yield 'no name supplied' => [
            '', []
        ];
        yield 'name with initial and surname' => [
            'Mr J. Smith', [
                [
                    'title' => 'Mr',
                    'first_name' => null,
                    'last_name' => 'Smith',
                    'initial' => 'J',
                ]
            ]
        ];
        yield 'first and surname' => [
            'Mister Joe Blogs', [
                [
                    'title' => 'Mister',
                    'first_name' => 'Joe',
                    'last_name' => 'Blogs',
                    'initial' => null,
                ]
            ]
        ];
        yield 'first and surname 2' => [
            'Mrs Jane Smith', [
                [
                    'title' => 'Mrs',
                    'first_name' => 'Jane',
                    'last_name' => 'Smith',
                    'initial' => null,
                ]
            ]
        ];
        yield 'two full names' => [
            'Mr Tom Staff and Mr P Doe', [
                [
                    'title' => 'Mr',
                    'first_name' => null,
                    'last_name' => 'Doe',
                    'initial' => 'P',
                ],
                [
                    'title' => 'Mr',
                    'first_name' => 'Tom',
                    'last_name' => 'Staff',
                    'initial' => null,
                ]
            ]
        ];
        yield 'two titles and one surname ' => [
            'Mr and Mrs Smith', [
                [
                    'title' => 'Mrs',
                    'first_name' => null,
                    'last_name' => 'Smith',
                    'initial' => null,
                ],
                [
                    'title' => 'Mr',
                    'first_name' => null,
                    'last_name' => 'Smith',
                    'initial' => null,
                ]
            ]
        ];
    }

    /**
     * @return Parser
     */
    private function getParser(): Parser
    {
        if (!$this->parser) {
            $this->parser = new Parser();
        }
        return $this->parser;
    }
}

