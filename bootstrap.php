<?php

error_reporting(E_ALL);

date_default_timezone_set('GMT');

// Check if Composer is installed
if (!is_dir(__DIR__ . '/vendor/')) {
    throw new Exception('No vendor folder detected. ' .
        'Please run composer install or check file permissions' . PHP_EOL);
}

require_once __DIR__ . "/vendor/autoload.php";