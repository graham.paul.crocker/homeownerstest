<?php

namespace Gpcrocker\Homeowner;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

/**
 * Class Log
 * @package Gpcrocker\Homeowner
 */
class Log
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * Method to return the Monolog instance
     *
     * @return \Monolog\Logger
     */
    static public function get()
    {
        if (!self::$instance) {
            self::configureInstance();
        }

        return self::$instance;
    }

    /**
     * Configure Monolog to use a rotating files system.
     *
     * @return Logger
     */
    protected static function configureInstance()
    {
        $dir = dirname(__DIR__) . '/var/log';

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $logger = new Logger('homeowner');
        $logger->pushHandler(new RotatingFileHandler($dir . DIRECTORY_SEPARATOR . 'info.log', 5));
        self::$instance = $logger;
    }

    /**
     * @param $message
     * @param array $context
     */
    public static function info($message, array $context = [])
    {
        self::getLogger()->addInfo($message, $context);
    }

    /**
     * @param $message
     * @param array $context
     */
    public static function error($message, array $context = [])
    {
        self::getLogger()->addError($message, $context);
    }
}
