<?php

namespace Gpcrocker\Homeowner;

/**
 * Class Parser
 * @package Gpcrocker\Homeowner
 */
class Parser
{
    /**
     * Pattern match Titles
     */
    const REGEX_TITLES = '/^Mrs|Mister|Dr|Prof|Ms|Mr|Miss/';
    /**
     * Pattern match Initials
     */
    const REGEX_INITIALS = '/^[a-zA-Z]{1}[\.]?\s/';
    /**
     * Pattern match an latin alphabet name
     */
    const REGEX_NAME = '/[a-zA-Z]{3,}/';
    /**
     * Pattern to split person
     */
    const REGEX_AMPERSAND = '/&|\sand\s/';

    /**
     * @param $filename
     */
    public function parseCsvData($filename)
    {
        $homeOwnerData = array_map('trim',
            str_getcsv(file_get_contents($filename))
        );
        // Remove Header
        array_pop($homeOwnerData);
        $homeOwnerData = array_filter(
            array_unique($homeOwnerData, SORT_REGULAR)
        );

        $homeOwnerArray = [];
        foreach ($homeOwnerData as $homeOwnerEntry) {
            $homeOwnerArray = array_merge($homeOwnerArray, $this->parsePerson($homeOwnerEntry));
        }
        return $homeOwnerArray;
    }


    /**
     * @param string $inputName
     */
    public function parsePerson(string $inputName): array
    {
        Log::get()->info("Parsing $inputName");
        if (!$inputName) {
            return [];
        }
        $inputName = trim($inputName);

        // Handle multiple homeowners
        if ($multiple = $this->matchPattern(self::REGEX_AMPERSAND, $inputName)) {
            $personArray = [];
            $lastNameEdgeCase = '';
            $splitNames = array_reverse(explode($multiple, $inputName));
            foreach ($splitNames as $splitName) {
                if (str_word_count($splitName) === 1) {
                    $splitName = $splitName . $lastNameEdgeCase;
                }
                $person = $this->parsePerson($splitName);
                $personResult = $person[0];
                $lastNameEdgeCase = $personResult['last_name'];
                $personArray[] = $personResult;
            }
            return $personArray;
        }

        // Optional Definitions
        $firstName = null;
        $initial = null;
        // Required Definitions
        $title = '';
        $lastName = '';

        $title = $this->matchPattern(self::REGEX_TITLES, $inputName);
        if ($title) {
            $inputName = trim(str_replace($title, '', $inputName));
        } else {
            Log::get()->error("$inputName does not have a title - skipping ");
            return [];
        }

        $initial = $this->matchPattern(self::REGEX_INITIALS, $inputName);
        if ($initial) {
            $inputName = trim(str_replace($initial, '', $inputName));
            $initial = str_replace(".", "", $initial);
        }

        $names = explode(' ', $inputName);
        if (count($names) >= 2) {
            $firstName = $names[0];
            $lastName = $names[count($names) - 1];
        } elseif (count($names) === 1) {
            $lastName = $names[0];
        } else {
            Log::get()->error("$inputName does not have a last name - skipping ");
            return [];
        }

        return [
            [
                'title' => $title,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'initial' => $initial
            ]
        ];
    }

    /**
     * @param string $pattern
     * @param $name
     * @return mixed|null
     */
    protected function matchPattern(string $pattern, $name)
    {
        preg_match($pattern, $name, $match);
        if ($match && count($match) > 0) {
            return trim($match[0]);
        }
        return null;
    }
}